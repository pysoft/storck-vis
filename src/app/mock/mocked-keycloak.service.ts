import { Injectable} from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import {KeycloakLoginOptions} from "keycloak-js";
import { environment } from '../../environments/environment';

@Injectable()
export class MockedKeycloakService extends KeycloakService {
  override init() {
    (this as any)._instance = this.getKeycloakInstance();
    return Promise.resolve(true);
  }

  override getKeycloakInstance() {
    return environment.keyCloakUser as any;
  }

  override login(options?: KeycloakLoginOptions | undefined) {
    return Promise.resolve();
  }

  override logout() {
    return Promise.resolve();
  }
}
