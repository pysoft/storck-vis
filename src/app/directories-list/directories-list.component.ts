import {
  Component,
  Input,
  OnInit,
  Renderer2,
  ViewChildren,
  QueryList,
  ElementRef, AfterViewInit,
} from "@angular/core";
import {StorckService} from "../core/storck.service";
import {Store} from "../core/store.service";
import {Actions} from "../core/actions.service";
import {File, Directory} from 'src/app/storck-defs';


interface DirectoryPayload extends Directory {
  loaded?: boolean;
  expanded?: boolean;
  active?: boolean;
  openPath?: string;
}

@Component({
  selector: 'directories-list',
  templateUrl: './directories-list.component.html',
  styleUrls: ['./directories-list.component.scss']
})
export class DirectoriesListComponent implements OnInit {
  @Input()
  path!: string;

  @Input()
  openPath: string | undefined;

  @ViewChildren('directoryName')
  directoryNameRefs!: QueryList<ElementRef<HTMLSpanElement>>;

  createDirectoryInputValue: string = '';

  directories: DirectoryPayload[] = [];

  ready: boolean = false;

  constructor(private storckService: StorckService, private store: Store, private actions: Actions, private renderer: Renderer2) {
    this.renderer.listen('window', 'click',(event: Event) => {
      this.directoryNameRefs.forEach((directoryNameRef, index) => {
        if (this.directories[index].active && event.target !== directoryNameRef.nativeElement) {
          // focus out
          //this.actions.changeActivePath('');
          //this.directories[index].active = false;
        }
      });
    });
  }

  ngOnInit(): void {
    if (this.store.workspace) {
      this.refresh(this.store.workspace.tokens[0].token, this.openPath);
    }

    this.store.changeWorkspaceEvent.subscribe(workspace => {
      if (!this.path) {
        this.refresh(workspace.tokens[0].token);
      }
    });

    this.store.changeActivePathEvent.subscribe(activePath => {
      this.directories.forEach(directory => {
        if (activePath === directory.path) {
          directory.active = true;
        } else if (directory.active) {
          directory.active = false;
        }
      });
    });

    this.store.openPathEvent.subscribe(path => {
      this.triggerOpenPath(path);
    });

    this.store.changeTabEvent.subscribe(tab => {
      if (tab === 'Directories' && this.path === this.store.activePath) {
        this.actions.changeViewedFiles(this.path)
      }
    });
  }

  triggerOpenPath(path: string) {
    const directoryToOpen = this.directories.find(directory => path.startsWith(directory.path));
    if (directoryToOpen && !directoryToOpen.expanded) {
      directoryToOpen.expanded = true;
      directoryToOpen.openPath = path;
      this.actions.changeActivePath(directoryToOpen.path);
    }
    else if (this.path === path) {
      this.actions.changeActivePath(path);
    }
  }

  refresh(token: string, openPath?: string) {
    this.storckService.getDirectories(this.path, token).subscribe((directories => {
      this.actions.updateDirectoriesFiles(this.path, this.excludeDirectories(directories));
      this.actions.changeViewedFiles(this.path);
      this.directories = directories;
      this.ready = true;
      if (openPath) {
        this.triggerOpenPath(openPath);
      }
    }));
  }

  toggleRow(directory: DirectoryPayload): void {
    directory.openPath = undefined;
    this.actions.changeActivePath(directory.path);
    directory.expanded = !directory.expanded;
    if (directory.expanded) {
      directory.loaded = true;
    }
  }

  nodeName(directory: DirectoryPayload): string {
    return directory.path.split('/').pop() || '';
  }

  excludeFiles(directories: DirectoryPayload[]): DirectoryPayload[] {
    return directories.filter(directory => !directory.file);
  }

  excludeDirectories(directories: DirectoryPayload[]): File[] {
    const files: File[] = [];
    directories.forEach(directory => {
      if (directory.file) {
        files.push(directory.file);
      }
    })
    return files;
  }
}
