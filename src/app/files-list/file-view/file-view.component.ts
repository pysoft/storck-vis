import {Component, OnInit} from '@angular/core';
import {Input} from '@angular/core';
import {File} from 'src/app/storck-defs';
import {StorckService} from "../../core/storck.service";
import {Store} from "../../core/store.service";
import {FilePayload} from "../files-list.component";


@Component({
  selector: 'file-view',
  templateUrl: './file-view.component.html',
  styleUrls: ['./file-view.component.scss']
})
export class FileViewComponent implements OnInit {

  @Input() file!: FilePayload;

  allVersions: File[] = [];

  constructor(private storckService: StorckService, private store: Store) { }

  ngOnInit(): void {
    if (this.file.previous_version) {
      this.allVersions = [this.file, ...this.file.previousVersions];
    }
  }

  selectVersion(file: File) {
    this.file = {...file, previousVersions: this.file.previousVersions};
  }

  download(event: Event, fileId: number, fileName: string) {
    event.stopPropagation();
    if (this.store.workspace) {
      this.storckService.downloadFile(fileId, fileName.replace(/^\//, ''), this.store.workspace.tokens[0].token)
        .subscribe();
    }
  }

  getKeys(object: Object): string[] {
      return Object.keys(object);
  }

  toArray(value: any) {
    return value instanceof Array ? value : [value];
  }

}
