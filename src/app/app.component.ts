import { Component } from '@angular/core';
import {StorckService} from "./core/storck.service";
import {Actions} from "./core/actions.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Storck-Vis';

  constructor() {}
}
