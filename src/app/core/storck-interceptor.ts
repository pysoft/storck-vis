import {Injectable} from "@angular/core";
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
import {from, mergeMap, Observable} from "rxjs";
import { environment } from '../../environments/environment';
import {KeycloakService} from "keycloak-angular";
import {Store} from "./store.service";

const API_URL: string = environment.apiUrl;

@Injectable()
export class StorckInterceptor implements HttpInterceptor {

  constructor(private keycloak: KeycloakService, private store: Store) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.keycloak.getToken()).pipe(
      mergeMap((token: string) => {
        const authReq = req.clone({
            url: API_URL + req.url,
            headers: req.headers.set('Authorization', 'Token ' + this.store.userToken),
          });
          return next.handle(authReq);
      })
    );
  }
}
