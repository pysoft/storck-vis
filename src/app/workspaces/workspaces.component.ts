import {Component, OnInit} from "@angular/core";
import {StorckService} from "../core/storck.service";
import {Actions} from "../core/actions.service";
import {Store} from "../core/store.service";
import {Workspace} from "src/app/storck-defs";
import {AddPersonDialogComponent} from "./add-person-dialog/add-person-dialog.component";
import {MatDialog} from "@angular/material/dialog";

interface WorkspacePayload extends Workspace {
  active?: boolean;
}

@Component({
  selector: 'workspaces',
  templateUrl: './workspaces.component.html',
  styleUrls: ['./workspaces.component.scss']
})
export class WorkspacesComponent implements OnInit {

  workspaces: WorkspacePayload[] = [];
  newWorkspaceInput = '';

  constructor(private storckService: StorckService,
              private store: Store,
              private actions: Actions,
              public dialog: MatDialog) {}

  ngOnInit(): void {
    this.refreshWorkspaces();

    this.store.changeWorkspaceEvent.subscribe(newWorkspace => {
      this.workspaces.forEach(workspace => {
        if (workspace.id === newWorkspace.id) {
          workspace.active = true;
        } else if (workspace.active) {
          workspace.active = false;
        }
      });
    });
  }

  refreshWorkspaces() {
    this.storckService.getWorkspaces().subscribe(workspaces => {
      this.workspaces = workspaces;
      if (this.workspaces.length) {
        this.actions.changeWorkspace(this.workspaces[0]);
      }
    });
  }

  changeActiveWorkspace(workspace: WorkspacePayload) {
    this.actions.changeWorkspace(workspace);
    this.actions.changeActivePath("");
  }

  addPerson(event: Event, workspace: Workspace) {
    event.stopPropagation();
    this.dialog.open(AddPersonDialogComponent, {
      width: '800px',
      data: workspace,
    });
  }

  addWorkspace() {
    this.storckService.createWorkspace(this.newWorkspaceInput).subscribe(() => this.refreshWorkspaces());
    this.newWorkspaceInput = '';
  }

}
