export interface File {
    id: number,
    stored_path: string,
    metadata?: any,
    date?: Date,
    hide?: boolean,
    previous_version?: number,
}

export interface Directory {
    id: number;
    path: string;
    workspace: number;
    file: File | null;
}

export interface WorkspaceToken {
    workspace_id: number;
    token: string;
}

export interface Workspace {
    id: number;
    name: string;
    users: {id: number, username: string}[];
    tokens: WorkspaceToken[];
}

export interface Schema {
  date?: string;
  filetype: string;
  id?: number;
  schema: any;
  workspace: number;
  user?: number;
}
