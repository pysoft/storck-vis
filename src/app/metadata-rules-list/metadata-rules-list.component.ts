import {Component} from "@angular/core";
import {StorckService} from "../core/storck.service";
import {Store} from "../core/store.service";
import {Schema} from "../storck-defs";
import {JsonEditorOptions} from "ang-jsoneditor";

@Component({
  selector: 'metadata-rules-list',
  templateUrl: './metadata-rules-list.component.html',
  styleUrls: ['./metadata-rules-list.component.scss']
})
export class MetadataRulesListComponent {

  schemas: Schema[] = [];
  activeSchema: Schema | undefined;
  editedSchemaJson: any;
  newFileType: string | undefined;
  canSave = false;
  options = new JsonEditorOptions();

  constructor(private storckService: StorckService, private store : Store) {
    this.options.modes = ['code', 'tree'];
    this.options.mode = 'code';
    store.changeWorkspaceEvent.subscribe(workspace => {
      this.reset();
      storckService.getSchema(workspace.tokens[0].token)
        .subscribe(schemas => {
          this.schemas = schemas.filter(schema => schema.workspace === workspace.id);
        });
    });
  }

  setActiveSchema(schema: Schema) {
    this.activeSchema = schema;
    this.editedSchemaJson = schema.schema
    this.newFileType = undefined;
    this.canSave = false;
  }

  editorChange(newValue: any) {
    this.editedSchemaJson = newValue;
    this.canSave = JSON.stringify(this.editedSchemaJson) !== JSON.stringify(this.activeSchema?.schema);
  }

  saveSchema() {
    if (this.store.workspace) {
      let filetype = '';
      if (this.newFileType !== undefined) {
        filetype = this.newFileType;
      }
      else if (this.activeSchema) {
        filetype = this.activeSchema.filetype;
      }
      const editedSchema = this.activeSchema;
      const newFileType = this.newFileType;
      const editedSchemaJson = this.editedSchemaJson;
      const workspace = this.store.workspace;
      this.storckService.setSchema(editedSchemaJson, filetype, workspace.tokens[0].token)
        .subscribe(() => {
          if (newFileType !== undefined) {
            this.schemas.push({
              filetype: newFileType,
              schema: editedSchemaJson,
              workspace: workspace.id,
            });
          }
          else if (editedSchema) {
            editedSchema.schema = editedSchemaJson;
          }
        });
      this.newFileType = undefined;
      this.canSave = false;
    }
  }

  addSchema() {
    this.activeSchema = undefined;
    this.newFileType = '';
    this.editedSchemaJson = {};
    this.canSave = false;
  }

  private reset() {
    this.newFileType = undefined;
    this.activeSchema = undefined;
    this.editedSchemaJson = {};
    this.canSave = false;
  }
}
